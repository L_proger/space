﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
using System;

public struct float2{
    public float2(float _x, float _y)
    {
        x = _x;
        y = _y;
    }
    public float x;
    public float y;
}

public struct double2 {
    public double2(double _x, double _y) {
        x = _x;
        y = _y;
    }
    public double x;
    public double y;
}

public enum AveragePositionFlag : byte {
    NoPositionData,
    NormalPosition,
    PhotocenterPosition
}

public enum Tycho2Solution {
    Normal,
    DoubleStar,
    Photocenter
}

public struct Tycho2Record {
    public int TYC1;
    public int TYC2;
    public int TYC3;
    public AveragePositionFlag AvgPositionFlag;
    public double2 AvgPositionJ2000; 
    public double2 AvgMotionJ2000;
    public float2 AvgPositionError;
    public float2 AvgMotionError;
    public float2 AvgPositionEpoch;
    public int UsedPositionsCount;
    public float2 AvgPositionGoodness;
    public float2 AvgMotionGoodness;
    public float BTMagnitude;
    public float BTError;
    public float VTMagnitude;
    public float VTError;
    public int Proximity;
    public bool Tycho1Exists;
    public long Hipparcos_ID;
    public string CCDM_ID;

    public double2 Tycho2PositionDeg;
    public float2 Tycho2Epoch;
    public float2 Tycho2Error;
    public Tycho2Solution Tycho2SolutionType;

    public float Tycho2PositionCorellation;

    public bool BTExists;
    public bool VTExists;
}

public struct CelestialCoordinate {
    public double a; //RA
    public double d; //DE
}

public class TychoEditor : EditorWindow {
    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Tycho")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        TychoEditor window = (TychoEditor)EditorWindow.GetWindow(typeof(TychoEditor));
        window.Show();
    }

    private static float2 ParseFloat2(string val)
    {
        var parts = val.Split(new [] {' '}, StringSplitOptions.RemoveEmptyEntries);
        return new float2(float.Parse(parts[0]), float.Parse(parts[1]));
    }

    private static double2 ParseDouble2(string val) {
        var parts = val.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        return new double2(double.Parse(parts[0]), double.Parse(parts[1]));
    }

    private void OnGUI() {
        if (GUILayout.Button("Combine files")) {
            string dir = @"C:\Users\Sergey\Downloads\I_259.tar\tycho2";
            var files = Directory.GetFiles(dir);
            Array.Sort(files);
            var result = File.Create(Path.Combine(dir, "tycho2.dat"));


            foreach (var f in files) {
                var data = File.ReadAllBytes(f);

                result.Write(data, 0, data.Length);
                Debug.Log(f);
            }
            Debug.Log((float)result.Position / (float)206);
            result.Close();

        }

        if (GUILayout.Button("Read lines")) {
            var dat = File.OpenRead(@"C:\Users\Sergey\Downloads\I_259.tar\tycho2\tycho2.dat");
            var datReader = new StreamReader(dat);
            //var lines = File.ReadAllLines();


            for (int i = 0; i < 10; ++i)
            {
                int p = 0;
                var line = datReader.ReadLine();
                Debug.Log(line);
                var parts = line.Split('|');

                Tycho2Record rec = new Tycho2Record();
                var tycId = parts[p++].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                rec.TYC1 = int.Parse(tycId[0]);
                rec.TYC2 = int.Parse(tycId[1]);
                rec.TYC3 = int.Parse(tycId[2]);

                rec.AvgPositionFlag = parts[p] == "P"
                    ? AveragePositionFlag.PhotocenterPosition
                    : (parts[p] == "X" ? AveragePositionFlag.NoPositionData
                    : AveragePositionFlag.NormalPosition);
                ++p;

                rec.AvgPositionJ2000.x = double.Parse(parts[p++]);
                rec.AvgPositionJ2000.y = double.Parse(parts[p++]);

                rec.AvgMotionJ2000.x = double.Parse(parts[p++]);
                rec.AvgMotionJ2000.y = double.Parse(parts[p++]);

                rec.AvgPositionError.x = float.Parse(parts[p++]);
                rec.AvgPositionError.y = float.Parse(parts[p++]);


                rec.AvgMotionError.x = float.Parse(parts[p++]);
                rec.AvgMotionError.y = float.Parse(parts[p++]);


                rec.AvgPositionEpoch.x = float.Parse(parts[p++]);
                rec.AvgPositionEpoch.y = float.Parse(parts[p++]);


                rec.UsedPositionsCount = int.Parse(parts[p++]);


                rec.AvgPositionGoodness.x = float.Parse(parts[p++]);
                rec.AvgPositionGoodness.y = float.Parse(parts[p++]);


                rec.AvgMotionGoodness.x = float.Parse(parts[p++]);
                rec.AvgMotionGoodness.y = float.Parse(parts[p++]);


                rec.BTMagnitude = float.Parse(parts[p++]);
                rec.BTError = float.Parse(parts[p++]);
                rec.VTMagnitude = float.Parse(parts[p++]);
                rec.VTError = float.Parse(parts[p++]);

                rec.Proximity = int.Parse(parts[p++]);

                var tyc = parts[p++];
                rec.Tycho1Exists = tyc == "T";

                var hip_val = parts[p++];

                if (long.TryParse(hip_val, out rec.Hipparcos_ID)) {
                    rec.CCDM_ID = parts[p++];
                } else {
                    //p++;
                }


                rec.Tycho2PositionDeg.x = double.Parse(parts[p++]);
                rec.Tycho2PositionDeg.y = double.Parse(parts[p++]);

                rec.Tycho2Epoch.x = float.Parse(parts[p++]);
                rec.Tycho2Epoch.y = float.Parse(parts[p++]);

                rec.Tycho2Error.x = float.Parse(parts[p++]);
                rec.Tycho2Error.y = float.Parse(parts[p++]);

                var postflg = parts[p++];
                rec.Tycho2SolutionType = postflg == "D" ? 
                    Tycho2Solution.DoubleStar:
                    (postflg == "P" ? 
                    Tycho2Solution.Photocenter : Tycho2Solution.Normal);


                rec.Tycho2PositionCorellation = float.Parse(parts[p++]);
                Debug.Log("TYC1: " + rec.TYC1);
                Debug.Log("TYC2: " + rec.TYC2);
                Debug.Log("TYC3: " + rec.TYC3);
            }
        }
    }
}